##########################################################################################
# EOS Python Connector Script
# Version 0.1.2.1
# Version Notes:
# Fixed Issue with SQL Query
# Authors:
# Jordan Maziarka
# Birket Engineering
# Copyright 2017
#########################################################################################

#The following are the imports needed for the script to run.
import socket
import json
import mysql.connector

#The variable below is used to store the query to insert into the mysql database
add_alarm = ("INSERT INTO alarms (alarmCode,locationText,locationId,description,timeDate,helpFileText,helpFileId,severity,severityId,discipline,isActive) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

#The following sets up the socket for the EOS Server
s = socket.socket()
host = '192.168.68.155'
port = 12345
s.bind((host,port))

#The following pauses before beginning the listen loop to wait for the socket to be created
s.listen(5)

#This while loop should run so long as the script is running so that it is always searching for new alarms
while True:

    #These lines will print out when it is awaiting a connection and will accept connections as they are found
    print('waiting for a connection')
    connection, client_address = s.accept()

    #This try loop will encapsulate the attempt to connect to the client
    try:

        #The following line will print out the address of the client that recently connected
        print('connection from',client_address)

        #The following while loop will encapsulate the search for data to then be logged into the database
        while True:

            #Recieving of data NOTE: Currently this is capped at 4096 Bytes, THIS HAS TO BE CHANGED IN THE VERY NEAR FUTURE
            data = connection.recv(4096)

            #The following if loop determines if data has been transmitted, and if it has will then insert into the database
            if data:

                #The following prints a status message and takes the TCP data and decodes it into a string
                print('logging alarm to database')
                message = data.decode("UTF-8")

                #The following decodes the JSON file, creates a python directory from it, and then inserts that database into a string for loading into the database
                pyMessage = json.loads(message)
                data_alarm = (pyMessage["alarmCode"],pyMessage["locationText"],pyMessage["locationId"],pyMessage["description"],pyMessage["timeDate"],pyMessage["helpFileText"],pyMessage["helpFileNo"],pyMessage["severity"],pyMessage["severityId"],pyMessage["discipline"],pyMessage["isActive"])

                #The following lines initiate the database connection, create the query cursor, execute the query, and then close the connection
                cnx = mysql.connector.connect(user="root",password="Frozen724816",database="EOS")
                cursor = cnx.cursor()
                cursor.execute(add_alarm, data_alarm)
                cnx.commit()
                cursor.close()
                cnx.close()

            #The following else loop handles the situation where a connection is opened and no data is transmitted
            else:
                print('no data from', client_address)
                break

    #The following will close the connection after data is done being sent            
    finally:
        connection.close()

